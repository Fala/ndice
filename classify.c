/* Algorytm Feature-Selection zbudowany na bazie klasyfikatora 2-Kostek
   Autor: Tomasz Falkiewicz <falkiewicz.tomek@gmail.com
   Data:  15/10/2014

   Ponizszy algorytm zwraca jakosc klasyfikatora uzyskanego przy pomocy
   2 wybranych zmiennych. Stanowi on roboczo-prototypowa wersje algorytmu
   adresowanego na procesory graficznego, bedacego tematem pracy dyplomowej. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <smmintrin.h>

/* Kompatybilnosc z C++ */
typedef int bool;
#define true 1
#define false 0

/* Opcje debugowania */
#define DEBUG_VEC false /* budowanie wektorow binarnych */
#define DEBUG_CLA false /* budowanie klasyfikatora */
#define DEBUG_DEC false /* obliczanie klasy decyzyjnej */
#define DEBUG_DIV false /* obliczanie podzialow losowych */
#define DEBUG_APR false /* obiekty w niewyczuczonych wokselach */
#define DEBUG_DEV false /* uzywane w miare potrzeb do aktywnego debugowania */

/* Opcjonalne dumpowanie klasyfikacjy zbioru uczacego do ./out/ */
#define DUMP true

/* Operacja popcount optymalizujaca zliczanie obiektow w wokselach */
#define POP_COUNT(x) _mm_popcnt_u64(x)

/* Typ potrzebny do posortowania obiektow po wartosciach danego atrybutu */
struct varObjPair {
  float value;
  int obj;  
};

/* Porownywanie obiektow przy sortowaniu po wartosci atrybutu */
int cmp_varObjPair (const void *a, const void *b) {
  if (((struct varObjPair*)a)->value < ((struct varObjPair*)b)->value) return -1;
  if (((struct varObjPair*)a)->value > ((struct varObjPair*)b)->value) return 1;
  return 0;
}

/* Porownywanie intow przy sortowaniu losowych podzialow */
int cmp_int (const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}

/* Klasyfikator elementarny */
typedef struct E_classifier {
  int    var[2];
  float* divArr[2];
  float* prob;
} E_classifier;

/* Klasyfikator czastkowy */
typedef struct M_classifier {
  int var[2];
  struct E_classifier** cs;
  int size;
} M_classifier;

/* Tworzenie tablicy podzialow (M-zmiennych x P-podzialow, dla N-obiektow
   i minimalnym podziale P_min) 

   POTENCJALNIE DO LICZENIA NA GPU */
int* random_divisions(int M, int N, int P, int P_min) {
  int* divs = (int*) malloc (sizeof(int) * M * P);
  int* tmp = (int*) malloc (sizeof(int) * P);
  int iP, iM;

  for (iM = 0; iM < M; iM++) {
    for (iP = 0; iP < P-1; iP++) {
      tmp[iP] = rand() % (N - P_min * P);
    } 
    qsort(tmp, P-1, sizeof(int), cmp_int);
    for (iP = 0; iP < P-1; iP++) {    
      divs[iM*P + iP] = P_min*(iP+1) + tmp[iP];
    }
    divs[iM*P + iP] = N-1;

    if (DEBUG_DIV){
      printf("%d: ", iM);
      for (iP = 0; iP < P-1; iP++)
        printf("%d,", divs[iM*P+iP]);
      printf("%d\n", divs[iM*P+iP]);
    }
  }

  free (tmp);  

  return divs; 
}

/* Budowa wektorow binarnych potrzebnych do zliczen obiektow w wokselach.
   * Wektory sa upakowane w ustrukturyzowana tablice:

     [zaznaczona zmienna(iV)] * M
       [podział po wartości(iP)] * P
         [klasa decyzyjna] * 2
           [bity w zmienej int] * V_len
             [1 - jezeli nalezy, 0 - jezeli nie nalezy]

   * Liczymy je tylko dla wybranych zmiennych.

   POTENCJALNIE DO LICZENIA NA GPU */
int** build_binVecArr(int *divArr, bool *varSelected, float *dataArr, 
                      int *decArr, struct varObjPair **varSorted, 
                      int P, int M, int N) {
  int **vecs;
  int iV, iP, iN, iO;
  int V_len;
  int position, shift;

  vecs = (int**) malloc (sizeof(int*) * M);
  V_len = (N / (sizeof(int) * 8))+1;
 
  for (iV = 0; iV < M; iV++)
    if (varSelected[iV]){
      vecs[iV] = (int*) malloc (sizeof(int) * P * 2 * V_len);
      memset(vecs[iV], 0, sizeof(int) * P * 2 * V_len);
      for (iP = 0, iN = 0; iP < P; iP++){
        for (; iN <= divArr[iV*P + iP]; iN++){
          iO = varSorted[iV][iN].obj;
          position = iP * (2*V_len) +     // iP-ty przedzial
            (decArr[iO] == 1) * V_len +   // klasa dec
            (iO / (sizeof(int)*8));       // int
          shift = (iO % (sizeof(int)*8)); // bit w int

          if (DEBUG_VEC) {
            printf("iV=%d\tiN=%d\tiO=%d\tiP=%d\tdivArr=%d\tpos=%d\tshift=%d\n", 
                  iV, iN, iO, iP, divArr[iV*P + iP], position, shift);
          }
          vecs[iV][position] |= (1 << shift);
        }
      } 
    }
  
  return vecs;
}

/* Budowa klasyfikatorow poprzez zliczenia. 
   Na razie wyswietla tylko statystyki.

   POTENCJALNIE DO LICZENIA NA GPU*/
struct E_classifier* build_E_classifier(int **binVecArr, int *varPair,
                                    int N, int M, int P, int *divArr,
                                    struct varObjPair **varSorted,
                                    float apriori) { 
  int iX, iY, iV, iP;
  int sum, count_pos, count_neg, sum_pos, sum_neg;
  int *A, *B;
  int V_len = (N / (sizeof(int) * 8))+1;

  E_classifier *c = (E_classifier*) malloc (sizeof(E_classifier));

  c->divArr[0] = (float*) malloc (P * sizeof(float));
  c->divArr[1] = (float*) malloc (P * sizeof(float));

  c->var[0] = varPair[0]; 
  c->var[1] = varPair[1]; 
  for (iP = 0; iP < P-1; iP++){
    c->divArr[0][iP] = varSorted[varPair[0]][divArr[varPair[0]*P + iP]].value;
    c->divArr[1][iP] = varSorted[varPair[1]][divArr[varPair[1]*P + iP]].value;
  }

  c->divArr[0][P-1] = 1.0;
  c->divArr[1][P-1] = 1.0;

  c->prob = (float*) malloc( P * P * sizeof(float));

  if (DEBUG_CLA) {
    printf("%d, %d:\n", varPair[0], varPair[1]);
  }

  for (iX = 0; iX < P; iX++){
    sum = 0; 
    sum_pos = 0; 
    sum_neg = 0;
    for (iY = 0; iY < P; iY++) {
      count_pos = 0;
      count_neg = 0;
      A = &binVecArr[varPair[0]][iX * 2*V_len];
      B = &binVecArr[varPair[1]][iY * 2*V_len]; 

      for (iV = 0; iV < V_len; iV++)
        count_neg += POP_COUNT(A[iV] & B[iV]);
      for (; iV < V_len * 2; iV++)
        count_pos += POP_COUNT(A[iV] & B[iV]); 

      if (count_pos + count_neg > 0) { 
        c->prob[iX*P + iY] = (float) count_pos / (count_pos + count_neg);    
      } else {
        if (DEBUG_APR){
          c->prob[iX*P + iY] = 666.0;
        } else {
          c->prob[iX*P + iY] = apriori;
        }
      }

      if (DEBUG_CLA) {
        printf("[%2d, %2d] = %d:%d {%.2f}\t", divArr[varPair[0] * P + iX], 
               divArr[varPair[1] * P + iY], count_pos, count_neg,
               c->prob[iX*P + iY]);

        sum_pos += count_pos;
        sum_neg += count_neg;
        sum += count_pos + count_neg;
      }
    }

    if (DEBUG_CLA) {
      printf("= %d(%d:%d)\n", sum, sum_pos, sum_neg);
    }
  }

  return c;
}

/* Klasyfikacja obiektu przy pomocy klasyfikatora elementarnego */
inline float E_classify(struct E_classifier *cs, float* obj, int P) {
  int iP;
  int v1, v2;

  for (iP = 0; iP < P; iP++)
    if (cs->divArr[0][iP] >= obj[cs->var[0]]) break;
  v1 = iP;

  for (iP = 0; iP < P; iP++)
    if (cs->divArr[1][iP] >= obj[cs->var[1]]) break;
  v2 = iP;

  return cs->prob[v1*P + v2];
}

/* Klasyfikacja obiektow przy pomocy klasyfikatora czastkowego */
float* M_classify(struct M_classifier *M_classifier, float* dataArr, 
                int dataN, int dataM, int M, int P) {
  int iN;
  int iM;
  float part;
  float *results = (float*) malloc (sizeof(float) * dataN);

  for (iN = 0; iN < dataN; iN++){
    part = 0.0;
    for (iM = 0; iM < M; iM++){
      part += E_classify(M_classifier->cs[iM], dataArr + iN*dataM, P);
    }
    results[iN] = part / M;
  }

  return results;
}

int main(int argc, char **argv){
  int M, P, P_min; 
  int iN, iM, iM2, iVN;
  FILE *varFile, *dataFile; 
  int *randDivArr;
  int **binVecArr;
  float *rank;
  int varM, varN;
  int dataM, dataN;
  int *varArr;
  float *dataArr;
  int *decArr;
  int i, j;
  int err = 0;
  int dec;
  char fileName[128];
  float apriori;
  float *estProb; 
  struct varObjPair **varSorted;
  bool *varSelected;
  struct M_classifier *M_classifiers;

  srand(time(0));

  if (argc != 8){
    fprintf(stderr, "%s <varFile> <dataFile> <mode> <M> <N> <P> <P_min>\n", 
            argv[0]);
    return 1;
  }

  /* Wczytujemy dane wejsciowe */
  varFile = fopen(argv[1], "r");
  dataFile = fopen(argv[2], "r");
  M = atoi(argv[4]);
  P = atoi(argv[6]);
  P_min = atoi(argv[7]);

  /* Jeszcze nie uzyte parametry */
  //mode = atoi(argv[3]);
  //N = atoi(argv[5]);

  fscanf(varFile, "%d,%d\n", &varM, &varN);
  varArr = (int*) malloc(sizeof(int) * varM * varN);

  if (varM == 2) {
    for (i = 0; i < varN; i++)
      fscanf(varFile, "%d,%d\n", 
             &varArr[i*varM], &varArr[i*varM+1]);
  } else {
    fprintf(stderr, "ERR: dopuszczalne tylko 2-ki\n");
    err = 1;
    goto cleanup_files;
  } 

  fscanf(dataFile, "%d,%d\n", &dataM, &dataN);

  apriori = 0.0;
  
  /* Wczytywanie danych do tablicy */
  dataArr = (float*) malloc(sizeof(float) * dataM * dataN);
  decArr = (int*) malloc(sizeof(int) * dataN);
  for (i = 0; i < dataN; i++){
    fscanf(dataFile, "%f", &dataArr[i*dataM]);
    for (j = 1; j < dataM; j++){
        fscanf(dataFile, ",%f", &dataArr[i*dataM + j]);
      }
    fscanf(dataFile, ",%d", &decArr[i]);
    if(decArr[i]) apriori ++;
    fscanf(dataFile, "\n");
  }  

  apriori /= dataN;

  /* Koniec wczytywania danych wejsciowych */
cleanup_files:
  fclose(varFile);
  fclose(dataFile);  

  if (err) {
    return 1;
  }

  /* allokujemy pamiec dla potrzebnych struktur */
  varSelected = (bool*) malloc (sizeof(bool) * dataM);
  varSorted = (struct varObjPair**) malloc (sizeof(struct varObjPair*) * dataM);
  rank = (float*) malloc (sizeof(float) * varN);

  /* zbieramy informacje o uzytych zmiennych */
  for (i = 0; i < dataM; i++) 
    varSelected[i] = 0;
  for (i = 0; i < varN*varM; i++) {
    varSelected[varArr[i]] = true;
  }

  /* Poczatek algorytmu klasyfikacji */

  // tworzymy posortowane pary (x_i, obiekt_i) (-> kernel?)
  for (j = 0; j < dataM; j++)
    if (varSelected[j]){
      varSorted[j] = (struct varObjPair*)
                       malloc (sizeof(struct varObjPair) * dataN);
      for (i = 0; i < dataN; i++){
        varSorted[j][i].value = dataArr[i*dataM + j];
        varSorted[j][i].obj = i;
      }

      qsort(varSorted[j], dataN, sizeof(struct varObjPair*), cmp_varObjPair);
    }

  // Alokujemy pamiec dla klasyfikatorów cząstkowych
  M_classifiers = (struct M_classifier*) malloc 
                  (sizeof(struct M_classifier) * varN);
  for (iVN = 0; iVN < varN; iVN++){
    M_classifiers[iVN].cs = (struct E_classifier**) malloc 
                            (sizeof(E_classifier*) * M);
    M_classifiers[iVN].var[0] = varArr[iVN * varM]; 
    M_classifiers[iVN].var[1] = varArr[iVN * varM + 1]; 
  }

  // for MonteCarlo
  for (iM = 0; iM < M; iM++){
    // tworzymy tabele podzialow (-> kernel)
    randDivArr = random_divisions(dataM, dataN, P, P_min);

    // tworzymy wektory binarne (-> kernel)
    binVecArr = build_binVecArr(randDivArr, varSelected, dataArr, decArr,
                                varSorted, P, dataM, dataN);

    for (iVN = 0; iVN < varN; iVN++){
      // wykonujemy operacje AND i POP_COUNT, zeby zliczyc gestosc wokseli
      // liczymy prawdopodobienstwa i estymacje klas dla wokseli
      // zapisujemy wyniki (-> kernel)
      M_classifiers[iVN].cs[iM] = build_E_classifier(binVecArr, &varArr[iVN*varM], 
                                                    dataN, dataM, P, randDivArr,
                                                    varSorted, apriori);
    }

    free (randDivArr);
    for (iM2 = 0; iM2 < dataM; iM2++)
      if (varSelected[iM2])
        free (binVecArr[iM2]);
    free (binVecArr);
  }

  // TODO: mozna od razu przepuszczac obiekty przez klasyfikator czastkowy
  //       dzieki czemu nie trzeba bedzie pamietac calego klasyfikatora!!!
  //       tzn. w petli wyzej zamiast tej nizej! TODO WAZNE !!!

  // dla kazdego ukladu (pary) zmiennych
  for (iVN = 0; iVN < varN; iVN++){
    estProb = M_classify(&M_classifiers[iVN], 
                         dataArr, dataN, dataM, M, P);
   
    if(DUMP){
      sprintf(fileName, "out/%d-%d.csv", M_classifiers[iVN].var[0],
                                         M_classifiers[iVN].var[1]);
      dataFile = fopen(fileName, "w");
    }

    rank[iVN] = 0;
    for (iN = 0; iN < dataN; iN++){
      if (estProb[iN] < 0.5) dec = 0;
      if (estProb[iN] >= 0.5) dec = 1;
      if (estProb[iN] == 666.0) dec = 3;
      if(DUMP) {
        for(iM = 0; iM < dataM; iM++){
          fprintf(dataFile, "%f,", dataArr[iN*dataM + iM]);
        }
        fprintf(dataFile, "%d,%d\n", dec, decArr[iN]);
      } // TODO SPRAWDZIC CZY (1-apriori) jest OK

      if(DEBUG_DEC) 
        printf("%1.3f ~ %s: ", estProb[iN], decArr[iN] ? "1" : "0");
      if (dec == decArr[iN]) {
        rank[iVN]++; 
        if(DEBUG_DEC) 
          printf("OK  ");
      } else {
        if(DEBUG_DEC) 
          printf("FAIL");
      }
      if(DEBUG_DEC)
        printf(" %1.4f\n", rank[iVN] / (iN+1));
    } 
    printf("%d, %d: %f\n", M_classifiers[iVN].var[0],
                           M_classifiers[iVN].var[1], rank[iVN] / dataN);
    free (estProb);
  }

  /* Koniec algorytmu klasyfikacji, sprzatanie pamieci */
  for (j = 0; j < dataM; j++)
    if (varSelected[j])
      free(varSorted[j]);

  for (iVN = 0; iVN < varN; iVN++) { 
    for (iM = 0; iM < M; iM++) {
      free (M_classifiers[iVN].cs[iM]->divArr[0]);
      free (M_classifiers[iVN].cs[iM]->divArr[1]);
      free (M_classifiers[iVN].cs[iM]->prob);
      free (M_classifiers[iVN].cs[iM]);
    }
    free (M_classifiers[iVN].cs);
  }
  free (M_classifiers);

  free (varSelected);
  free (varSorted);
  free (rank);
  free (decArr);
  free (varArr);
  free (dataArr);

  return err;
}
