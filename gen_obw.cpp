#include<cstdio>
#include<ctime>
#include<cstdlib>


float randFloat(float min, float max) {
	float rand01 = rand()/(float)RAND_MAX;
	float random = rand01*(max-min) + min;
	return random;
}

int main(int argc, char *argv[]) {
	unsigned int seed = atoi(argv[3]);
	srand(seed);
	int K = atoi(argv[1]);
	int N = atoi(argv[2]);

	printf("%d,%d\n", K, N);

	for(int i=0;i<N;i++) {
		float var1;
		float var2;
		for(int j=0 ;j < K ; j++) {
			float var = randFloat(0.0f, 1.0f);
			if (j==0) var1 = var;
			if (j==1) var2 = var;
			printf("%f,", var);
		}
    float dist = (0.5 - var1)*(0.5 - var1) + (0.5 - var2)*(0.5 - var2);
    bool decision = (dist < 0.20 && dist > 0.01) ? true : false;

  	printf("%d\n", decision);

	}

	return 0;
}
