set term png
set output df.".png"
set datafile separator ","
set xrange [0:1]
set yrange [0:1]

plot df.".csv" using 1:($5==1?$2:1/0) notitle w p pt 9 lc 2,\
     df.".csv" using 1:($5==0?$2:1/0) notitle w p pt 8 lc 2,\
     df.".csv" using 1:($5!=$6?$2:1/0):(0.012) with circles lc rgb "red" notitle,\
     df.".csv" using 1:($5==3?$2:1/0) notitle w p pt 5 lc 3;
      
