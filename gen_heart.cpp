#include<cstdio>
#include<ctime>
#include<cstdlib>
#include<cmath>

float randFloat(float min, float max) {
	float rand01 = rand()/(float)RAND_MAX;
	float random = rand01*(max-min) + min;
	return random;
}

int main(int argc, char *argv[]) {
	unsigned int seed = atoi(argv[3]);
	srand(seed);
	int K = atoi(argv[1]);
	int N = atoi(argv[2]);

	printf("%d,%d\n", K, N);

	for(int i=0;i<N;i++) {
		float var1;
		float var2;
		for(int j=0 ;j < K ; j++) {
			float var = randFloat(0.0f, 1.0f);
			if (j==0) var1 = var;
			if (j==1) var2 = var;
			printf("%f,", var);
		}
   
    var1 = (var1*3-1.5); 
    var2 = (var2*2.5-1.05);
    bool decision = pow(pow(var1,2)+pow(var2,2)-1,3) - pow(var1,2)*pow(var2,3) < 0;

  	printf("%d\n", decision);

	}

	return 0;
}
