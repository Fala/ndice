/*
Generator danych testowych do zadania nr 1

Parametry wejsciowe:
K - liczba zmiennych
N - liczba obiektow
seed - seed dla generatora liczb losowych

Wyjscie:
Program wypisuje na standardowe wyjscie:
w pierwszej linii:
K N
dalej N linii (po 1 dla kazdego obiektu)
w kazdej linii K liczb typu float (wartosci zmiennych) oraz zmienna decyzyjna {0,1}

Przyklad wejscia:
4 5 0

Przykład wyjścia:
4 5
0.840188 0.394383 0.783099 0.798440 1
0.911647 0.197551 0.335223 0.768230 1
0.277775 0.553970 0.477397 0.628871 1
0.364784 0.513401 0.952230 0.916195 1
0.635712 0.717297 0.141603 0.606969 0

Ogolny opis:
Z zalozenia ten kod powinien generowac dane dla ktorych najwieksza istotnosc
ma para zmiennych (0,1).
*/

#include<cstdio>
#include<ctime>
#include<cstdlib>


float randFloat(float min, float max) {
	float rand01 = rand()/(float)RAND_MAX;
	float random = rand01*(max-min) + min;
	return random;
}

int main(int argc, char *argv[]) {
	unsigned int seed = atoi(argv[3]);
	srand(seed);
	int K = atoi(argv[1]);
	int N = atoi(argv[2]);

	printf("%d,%d\n", K, N);

	for(int i=0;i<N;i++) {
		float var1;
		float var2;
		for(int j=0 ;j < K ; j++) {
			float var = randFloat(0.0f, 1.0f);
			if (j==0) var1 = var;
			if (j==1) var2 = var;
			printf("%f,", var);
		}
		bool test1 = var1 > 0.5f;
		bool test2 = var2 > 0.5f;

		bool decision = test1^test2;
		printf("%d\n", decision);

	}

	return 0;
}
