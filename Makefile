CC = gcc
CXX = g++
CFLAGS = -Wall -ggdb3 -march=native -mtune=native
EXE = classify gen_xor gen_obw gen_heart gen_xor_obw

all: $(EXE)

classify: classify.c
	$(CC) $(CFLAGS) $< -o $@

gen_xor: gen_xor.cpp
	$(CXX) $(CFLAGS) $< -o $@

gen_obw: gen_obw.cpp
	$(CXX) $(CFLAGS) $< -o $@

gen_heart: gen_heart.cpp
	$(CXX) $(CFLAGS) $< -o $@

gen_xor_obw: gen_xor_obw.cpp
	$(CXX) $(CFLAGS) $< -o $@

clean:
	rm $(EXE) 2> /dev/null
